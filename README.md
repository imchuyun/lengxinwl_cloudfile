# 冷心云File文件系统</br>
</br>[最新版本V2021.1 | ](https://gitee.com/lengxinwl/lengxinwl_cloudfile/issues)
[提交issues | ](https://gitee.com/lengxinwl/lengxinwl_cloudfile/issues)
[加入交流群 | ](https://jq.qq.com/?_wv=1027&k=H3oxx0ql)
[联系商务QQ | ](https://jq.qq.com/?_wv=1027&k=H3oxx0ql)
[访问官网](http://www.6661314.xyz)
#### 版本
> 冷心云文件系统（PHP）V2021.1.1【第1次更新】




PS:原创首发</br>
下载地址:[V2021.PHP+nginx版本](https://gitee.com/lengxinwl/lengxinwl_cloudfile/releases/V2021.1.1)




![QQ交流群](https://images.gitee.com/uploads/images/2021/0102/115511_fa3fde25_1859782.jpeg "QQ图片20210101230039.jpg")

#### 功能
> 本程序由php开发，建议搭载在nginx上，当然apace也行，请自测！
①可上传php、图片、压缩包等，全部后缀已支持！
②能够配置多用户系统，可给自己内部的人使用
③有文件列表功能，在linux上使用也不过分
④搭建记录一目了然，用户搭建后可以自己去看
⑤支持“普通上传”和“MD5上传”，懒人必备
⑥界面使用layui美化，可打开css自己调色


#### 介绍
欢迎使用冷心云文件系统（PHP）</br>
该源码由冷心云（初云）提供，您可自行搭建在虚机、服务器上等，详情配置请看下面：


#### 安装教程

1.  首先先从gitee把文件先下载下来，你可以用git也可以在网页直接下载zip
2.  在服务器或者虚机创建一个网站并且绑定域名（本源码不需要mysql数据库，无需创建一个数据库）
3.  上传源码到网站空间内，请使用zip上传后解压方式，切记注意不要少文件
4.  给下目录权限777
5.  我们打开“config.php”配置下文件：

```
<?
    global $switch;
    global $name;
    global $version;
    global $url;
    $switch = true; //这里是网站的开关，true开启，false关闭
    $name = "冷心云(桔籽)内部上传系统V2021.1"; //站点名称
    $version = "v2021.1.1"; //版本号
    $url = "http://web.6661314.xyz"; //网站地址（切记后面不能带/，记得加http，可以是https）
?>
```
6.  记得配置登录账号，打开“login_user.php”配置下文件：
</br>（注意：账号的密码是MD5加密的，需要自己去加密，格式：账号|密码，多账号的时候记得加英文逗号 , ）

```
<?
	$login=[
			"admin|672da9e96f92cd1e7115c553fbaaeabd",
			"vip|26061ff43a17e60a86ecac82f067483f",
			"test|123"
		  ]
?>
```




#### 注意事项

1.  本源码由冷心云（初云）原创，如您需要商业用途请联系商务QQ：2135820046，交流群：[826066209](https://jq.qq.com/?_wv=1027&k=xMRIges5)
2.  若出现上传问题请加入交流群，亲测是正常的，可能与网速有关
3.  界面可以自行美化，无加密
4.  出现技术问题请提交[issues](https://gitee.com/lengxinwl/lengxinwl_cloudfile/issues)

#### 感谢贡献

1.  Layui（界面）
2.  magicalcoder（可视化界面设计）
3.  nginx、php（网站支持）
4.  vscode（编辑器）


#### 关于我们
★ 博主想开个技术社区，不知道有没这样必要，欢迎加入讨论
[官方网站] ： [WWW.6661314.XYZ](http://WWW.6661314.XYZ)
[商务网站] ： [B.6661314.xyz](http://B.6661314.xyz)
[我的博客] ： [i.6661314.xyz](http://i.6661314.xyz)
★ 本程序由冷心云提供技术服务，欢迎提技术、界面、功能建议
