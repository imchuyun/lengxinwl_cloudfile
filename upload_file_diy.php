<?php
// 允许上传的图片后缀
session_start();//启用session
	header("Content-type:text/html;charset=utf-8");
	date_default_timezone_set('PRC'); //调整时区
	if (!empty($_SESSION["time"])){
		deal($_POST);
	}else{
		header("Refresh:0;url=login.html");
	}
 global $context;

function deal($post){
	global $context;
	$post=$_POST;
	//print_r($post);
	if($post["filecode"]==""){
		$context=$context. "请输入文件标记";
	}else{
		if(preg_match("/[\x7f-\xff]/", $post["filecode"])){
			if($post["drivename"]==""){
				$context=$context. "请输入目录名称";
			}else{
				include __DIR__.'/config.php';
				if(preg_match("/[\x7f-\xff]/", $post["drivename"])){
					$context=$context. "目录名称必须为英文字符（英文、数字、下划线）";
				}else{
					$path="upload/".$post["drivename"];
					if (is_dir($path)){  
						$context=$context. "目录: " . $path . " 已经存在，系统会将文件上传到该目录</br>";
						upload($path,$post["switch"],$url,$post["filecode"]);
					}else{
						$res=mkdir(iconv("UTF-8", "GBK", $path),0777,true);
						if ($res){
							$context=$context. "目录: ".$path." 创建成功</br>";
							upload($path,$post["switch"],$url,$post["filecode"]);
						}else{
							$context=$context. "目录: ".$path." 创建失败</br>";
						}						
						
					}
				}
			}
		}else{
			$context=$context. "文件标记必须为中文字符（上传后文件名不变）";
		}
	}
}
function upload($path,$zip,$url,$code){
	global $context;
//$allowedExts = array("php","html","zip", "txt","png",);
$temp = explode(".", $_FILES["file"]["name"]);
$context=$context. $_FILES["file"]["size"];
$extension = end($temp);     // 获取文件后缀名
if ($_FILES["file"]["size"] < 5242880)   // 小于 5MB
{
	if ($_FILES["file"]["error"] > 0)
	{
		$context=$context. "错误: " . $_FILES["file"]["error"] . "<br>";
	}
	else
	{
		$context=$context. "上传文件名: " . $_FILES["file"]["name"] . "<br>";
		$context=$context. "文件类型: " . $_FILES["file"]["type"] . "<br>";
		$context=$context. "文件大小: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
		//$context=$context. "文件临时存储的位置: " . $_FILES["file"]["tmp_name"] . "<br>";
		
		// 判断当期目录下的 upload 目录是否存在该文件
		// 如果没有 upload 目录，你需要创建它，upload 目录权限为 777
		if (file_exists($path."/" . $_FILES["file"]["name"]))
		{
			$context=$context. $_FILES["file"]["name"] . " 文件已经存在，即将替换文件！<br>";
			
			move_uploaded_file($_FILES["file"]["tmp_name"], $path."/". $_FILES["file"]["name"]);
			$context=$context. "文件存储在: " . $path."/" . $_FILES["file"]["name"]."<br>";
			if($zip=='on'){
				$context=$context. "系统将在3秒后开始解压，请不要关闭页面！<br>";
				$zip_file=$path."/" . $_FILES["file"]["name"];
				$zip_path=$path."/";
				require 'unzip.php';
			}
			$context=$context. "================</br>";
			$context=$context. "</br>[访问地址]</br></br>".$url."/".$path."/" . $_FILES["file"]["name"]."</br>";
			$context=$context. "</br>";
			$site_url=$url."/".$path."/" . $_FILES["file"]["name"];
			$site_name=$code;
			$site_time=date("Y/m/d H:i:s");
			require 'site.php';
			drivelist(__DIR__."\\".strtr($path,'/',"\\"));
		}
		else
		{
			// 如果 upload 目录不存在该文件则将文件上传到 upload 目录下
			move_uploaded_file($_FILES["file"]["tmp_name"], $path."/". $_FILES["file"]["name"]);
			$context=$context. "文件存储在: " . $path."/" . $_FILES["file"]["name"]."<br>";
			if($zip=="on"){
				$context=$context. "系统将在3秒后开始解压，请不要关闭页面！<br>";
				$zip_file=$path."/" . $_FILES["file"]["name"];
				$zip_path=$path."/";
				require 'unzip.php';
			}
			$context=$context. "================</br>";
			$context=$context. "</br>[访问地址]</br></br>".$url."/".$path."/" . $_FILES["file"]["name"]."</br>";
			$context=$context. "</br>";
			$site_name=$code;
			$site_url=$url."/".$path."/" . $_FILES["file"]["name"];
			$site_time=date("Y/m/d H:i:s");
			require 'site.php';
			drivelist(__DIR__."\\".strtr($path,'/',"\\"));
			
		}
	}
}
else
{
	$context=$context. "非法的文件格式";
}
}
function drivelist($path){
	global $context;
	$context=$context. "[文件展示]</br>";
	$dr = @opendir($path);
   if(!$dr) {
     $context=$context. "Error opening the ".$path." directory!<BR>";
     exit;
   }
 
   while(($files[] = readdir($dr)) !== false);
 
   foreach($files as $value){
	   $context=$context. $value."</br>";
   }

}
echo '<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" /><style>html,body,iframe{width:100%;height:99.5%;padding:0;margin:0}#wrap{width:100%;height:100%}iframe{border:0}</style><div id="wrap"><iframe src="/tips.php?context='.$context.'"></iframe></div></html>';
?>